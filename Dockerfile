FROM ghcr.io/avucoh/dive:latest

# DataView image is built on top of the DIVE-rocker image

# Copy config for server to image
COPY shiny-server.conf /etc/shiny-server/shiny-server.conf

# Put relevant app assets in app directory in image
ADD appdata.tar.gz /srv/shiny-server/dataview
COPY db /srv/shiny-server/dataview/db
COPY db.wal /srv/shiny-server/dataview/db.wal
COPY R /srv/shiny-server/dataview/R
COPY www /srv/shiny-server/dataview/www
COPY global.R /srv/shiny-server/dataview
COPY server.R /srv/shiny-server/dataview
COPY ui.R /srv/shiny-server/dataview
COPY ui_home.R /srv/shiny-server/dataview

# Set permissions
RUN sudo chown -R shiny:shiny /srv/shiny-server

# Expose Shiny's port
EXPOSE 8080

# Run app
ENTRYPOINT ["/usr/bin/shiny-server.sh"]

# To see app, start a container with:
# docker run -d --rm -p 8080:8080 npod/dataview
