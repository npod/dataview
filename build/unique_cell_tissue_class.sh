#!/bin/bash

# Compose unique list of cell tissue (column 13) from feature metadata
touch cell_tissue_classes
for meta in metadata/feature/*; do
  awk -F',' '{ if (NR>1) print $13}' $meta >> cell_tissue_classes
done
sort cell_tissue_classes | uniq -c > test
