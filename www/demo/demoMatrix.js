// handler demoMatrix
Shiny.addCustomMessageHandler("demoMatrix",
  function(message) {
    var intro = introJs();
    var userDrill = function(event) {
        if(event.target.id === 'nPOD-dd-scatter') { 
          intro.nextStep();
        }
    };
    $(document).on('shiny:recalculated', userDrill);
    intro.setOptions({
      steps: message.steps,
      'showStepNumbers': false,
      'hidePrev': true,
      'hideNext': true,
      'showBullets': false,
      'highlightClass': 'custom-highlight',
      'doneLabel': 'Upload demo dataset & exit demo', // 'Upload demo dataset'
      'exitOnEsc': true
    })
    .start()
    .onafterchange(function(targetElement) {
        if(this._currentStep === 2) { $(document).off('shiny:recalculated', userDrill) }
    })
    .oncomplete(function() {
      Shiny.setInputValue("nPOD-upload-appdata", "appdata/emcounts_summarized.csv" );
    })
    .onexit(function() { $(document).off('shiny:recalculated', userDrill) });
});
