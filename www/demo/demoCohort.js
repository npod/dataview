// handler demoCohortExchange
Shiny.addCustomMessageHandler("demoCohortExchange",
  function(message) {
    $(".nav-tabs a[data-value='Reference graph']").tab('show');
    var intro = introJs();
    var firststeps = message.steps.slice(0,3);
    intro.setOptions({
      steps: firststeps,
      'showStepNumbers': false,
      'hidePrev': true,
      'hideNext': true,
      'showBullets': false,
      'highlightClass': 'custom-highlight',
      'exitOnOverlayClick': false,
      'doneLabel': 'Upload demo dataset' })
    .start()
    .oncomplete( function() {
      Shiny.setInputValue("nPOD-match-CohortX-upload-appdata", "appdata/divid_coded.csv");
      setTimeout(function() { introCX2(message) }, 250);
    });
});

function introCX2(message) {
  var intro = introJs();
  $(".nav-tabs a[data-value='Match parameters']").tab('show');
  var nextsteps = message.steps.slice(3,5);
  intro.setOptions({
    steps: nextsteps,
    'showStepNumbers': false,
    'hidePrev': true,
    'hideNext': true,
    'showBullets': false,
    'doneLabel': 'Try exploring data <strong><em>without</em></strong> matching'
  })
  .start()
  .oncomplete( function() {
    $(".nav-tabs a[data-value='Explore']").tab('show');
    setTimeout(function() { introCX3(message) }, 250);
  });
}

function introCX3(message) {
  var intro = introJs();
  var nextsteps = message.steps.slice(5,6);
  intro.setOptions({
    steps: nextsteps,
    'showStepNumbers': false,
    'hidePrev': true,
    'hideNext': true,
    'showBullets': false,
    'doneLabel': 'OK, try matching on some attributes'
  })
  .start()
  .oncomplete( function() {
    $(".nav-tabs a[data-value='Match parameters']").tab('show');
    setTimeout(function() { introCX4(message) }, 500);
  });
}

// need to listen for run-params by user
function introCX4(message) {
  var intro = introJs();
  var userRun = function(event) {
    if(event.target.id === 'nPOD-match-results-table') {
      $(".nav-tabs a[data-value='Match results']").tab('show');
      intro.exit();
    }
  };
  $(document).on('shiny:value', userRun);
  var nextsteps = message.steps.slice(6,10);
  intro.setOptions({
    steps: nextsteps,
    'showStepNumbers': false,
    'showBullets': false,
    'doneLabel': 'Skip'
  })
  .start()
  .onexit(function() {
    $(document).off('shiny:value', userRun);
    setTimeout(function() { introCX5(message) }, 250);
  });
}

function introCX5(message) {
  var intro = introJs();
  var nextsteps = message.steps.slice(10,11);
  intro.setOptions({
    steps: nextsteps,
    'showStepNumbers': false,
    'showBullets': false,
    'hidePrev': true,
    'hideNext': true,
    'doneLabel': 'Explore data <strong><em>after</em></strong> matching',
  })
  .start()
  .oncomplete(function() {
    $(".nav-tabs a[data-value='Explore']").tab('show');
    setTimeout(function() { introCX6(message.steps.slice(11,12)) }, 500);
  });
}

function introCX6(nextsteps) {
  var intro = introJs();
  // var nextsteps = message.steps.slice(11,12);
  intro.setOptions({
    steps: nextsteps,
    'showStepNumbers': false,
    'showBullets': false,
    'hidePrev': true,
    'hideNext': true,
    'doneLabel': 'Done with demo'
  })
  .start();
  // .oncomplete( function() {
  //  Shiny.setInputValue("nPOD-match-CohortX-name", "");
  //});
}


